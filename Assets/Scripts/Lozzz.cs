using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lozzz : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        Balls ball = other.gameObject.GetComponent<Balls>();
        if (ball != null)
        {
            ball.Invalue(true);
        }
    }
}
